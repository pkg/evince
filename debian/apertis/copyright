Format: https://www.debian.org/doc/packaging-manuals/copyright-format/1.0/

Files: *
Copyright: no-info-found
License: GPL

Files: AUTHORS
Copyright: 1996-2003, Glyph & Cog, LLC.
License: UNKNOWN

Files: CONTRIBUTING.md
Copyright: attribution.
License: UNKNOWN

Files: backend/*
Copyright: 2006-2010, 2012, 2013, 2015, Carlos Garcia Campos <carlosgc@gnome.org>
License: GPL-2+

Files: backend/comics/*
Copyright: 2017, Bastien Nocera <hadess@hadess.net>
License: GPL-2+

Files: backend/comics/comics-document.c
Copyright: 2016, 2017, Bastien Nocera <hadess@hadess.net>
 2009, 2010, Juanjo Marín <juanj.marin@juntadeandalucia.es>
 2005, Teemu Tervo <teemu.tervo@gmx.net>
License: GPL-2+

Files: backend/comics/comics-document.h
Copyright: 2005, Teemu Tervo <teemu.tervo@gmx.net>
License: GPL-2+

Files: backend/djvu/*
Copyright: 2006, Michael Hofmann <mh21@piware.de>
License: GPL-2+

Files: backend/djvu/djvu-document.c
 backend/djvu/djvu-document.h
Copyright: 2005, Nickolay V. Shmyrev <nshmyrev@yandex.ru>
License: GPL-2+

Files: backend/djvu/djvu-links.c
 backend/djvu/djvu-links.h
Copyright: 2006, Pauli Virtanen <pav@iki.fi>
License: GPL-2+

Files: backend/dvi/dvi-document.c
 backend/dvi/dvi-document.h
Copyright: 2005, Nickolay V. Shmyrev <nshmyrev@yandex.ru>
License: GPL-2+

Files: backend/dvi/mdvi-lib/*
Copyright: 2000, Matias Atria
License: GPL-2+

Files: backend/dvi/mdvi-lib/afmparse.c
 backend/dvi/mdvi-lib/afmparse.h
Copyright: 1988-1990, Adobe Systems Incorporated.
License: Afmparse

Files: backend/dvi/texmfcnf.c
 backend/dvi/texmfcnf.h
Copyright: 2010, Hib Eris <hib@hiberis.nl>
License: GPL-2+

Files: backend/pdf/*
Copyright: 2004, 2005, 2011, 2012, 2015, 2016, Red Hat, Inc.
License: GPL-2+

Files: backend/pdf/ev-poppler.c
Copyright: 2018, Evangelos Rigas <erigas@rnd2.org>
 2009, Juanjo Marín <juanj.marin@juntadeandalucia.es>
 2004, Red Hat, Inc.
License: GPL-2+

Files: backend/ps/ev-spectre.h
Copyright: 1998-2005, The Free Software Foundation
License: LGPL-2+

Files: backend/tiff/*
Copyright: 2005, Jonathan Blandford <jrb@gnome.org>
License: GPL-2+

Files: backend/tiff/tiff2ps.c
Copyright: 1991-1997, Silicon Graphics, Inc.
 1988-1997, Sam Leffler
License: libtiff

Files: backend/tiff/tiff2ps.h
Copyright: 2005, rpath, Inc.
License: GPL-2+

Files: cut-n-paste/*
Copyright: 2004, 2005, 2011, 2012, 2015, 2016, Red Hat, Inc.
License: GPL-2+

Files: cut-n-paste/gimpcellrenderertoggle/*
Copyright: 2003, 2004, Sven Neumann <sven@gimp.org>
 1995-1997, Peter Mattis and Spencer Kimball
License: LGPL-2+

Files: cut-n-paste/libdazzle/*
Copyright: 2020, Germán Poo-Caamaño <gpoo@gnome.org>
 2015-2017, Christian Hergert <christian@hergert.me>
 1995-2017, GIMP Authors
License: GPL-3+

Files: cut-n-paste/libdazzle/dzl-file-manager.h
Copyright: 2015, Christian Hergert <christian@hergert.me>
License: GPL-3+

Files: cut-n-paste/libdazzle/dzl-version-macros.h
Copyright: 2017, Christian Hergert <chergert@redhat.com>
License: GPL-3+

Files: cut-n-paste/libgd/gd-two-lines-renderer.c
 cut-n-paste/libgd/gd-two-lines-renderer.h
Copyright: 2011, Red Hat, Inc.
License: LGPL-2+

Files: data/*
Copyright: UNKNOWN
License: UNKNOWN

Files: debian/*
Copyright: file is more complex, but correct:
License: UNKNOWN

Files: debian/evince-thumbnailer.xml
Copyright: 2007, Marc Brockschmidt <he@debian.org>
License: GPL-2+

Files: debian/evince.apport
Copyright: 2009-2011, Canonical Ltd.
License: UNKNOWN

Files: debian/evince.xml
Copyright: 2005, Lars Wirzenius (liw@iki.fi)
License: GPL-2+

Files: debian/tests/*
Copyright: 2018, 2019, Simon McVittie
 2012, Canonical Ltd.
License: UNKNOWN

Files: evince-document.h
 evince-view.h
Copyright: 2009, 2012, 2018, Christian Persch
License: LGPL-2.1+

Files: help/*
Copyright: 2010, 2012, 2017, 2021, 2022, evinces COPYRIGHT HOLDER
License: UNKNOWN

Files: help/bg/*
Copyright: no-info-found
License: GFDL-1.1+

Files: help/en_GB/*
Copyright: 2006, THE PACKAGES COPYRIGHT HOLDER
License: GFDL-1.1+

Files: help/fi/*
Copyright: Tommi Vainikainen, 2006.
 Timo Jyrinki, 2008.
 Flammie Pirinen, 2006. (merged from gucharmap translation)
License: UNKNOWN

Files: help/fr/*
Copyright: 2006-2022, Free Software Foundation, Inc.
License: UNKNOWN

Files: help/hu/*
Copyright: 2011-2019, 2021-2022
License: UNKNOWN

Files: help/it/*
Copyright: 2006, Free Software Foundation, inc.
License: UNKNOWN

Files: help/ja/*
Copyright: 2012, the Japnese translators of help for evince
License: UNKNOWN

Files: help/ko/*
Copyright: 2012, Carlos Garcia Campos et al.
License: UNKNOWN

Files: help/oc/*
Copyright: 2006, 2007, Free Software Foundation, Inc.
License: GFDL-1.1+

Files: help/pl/*
Copyright: 2017-2021, the evince authors.
License: UNKNOWN

Files: help/pt_BR/*
Copyright: 2006-2022, Free Software Foundation, Inc.
License: UNKNOWN

Files: help/ru/*
Copyright: 2008, Free Software Foundation Inc.
License: UNKNOWN

Files: help/sr/*
Copyright: no-info-found
License: GFDL-1.1+

Files: help/sv/*
Copyright: 2006-2022, Free Software Foundation, Inc.
License: UNKNOWN

Files: help/te/*
Copyright: 2011, 2012, Swecha Telugu Localisation Team <localization@swecha.net>
License: UNKNOWN

Files: help/uk/*
Copyright: 2005, Free Software Foundation
License: UNKNOWN

Files: help/vi/*
Copyright: no-info-found
License: GFDL-1.1+

Files: help/zh_CN/*
Copyright: 2006-2022, Free Software Foundation, Inc.
License: UNKNOWN

Files: help/zh_HK/*
Copyright: 2011, evinces COPYRIGHT HOLDER
License: GFDL-1.1+

Files: help/zh_TW/*
Copyright: 2011, evinces COPYRIGHT HOLDER
License: GFDL-1.1+

Files: libdocument/*
Copyright: 2006-2010, 2012, 2013, 2015, Carlos Garcia Campos <carlosgc@gnome.org>
License: GPL-2+

Files: libdocument/ev-annotation.c
 libdocument/ev-annotation.h
 libdocument/ev-document-annotations.c
Copyright: 2009, Carlos Garcia Campos <carlosgc@gnome.org>
 2007, Iñigo Martinez <inigomartinez@gmail.com>
License: GPL-2+

Files: libdocument/ev-async-renderer.c
 libdocument/ev-async-renderer.h
 libdocument/ev-document-misc.h
 libdocument/ev-selection.h
Copyright: 2000-2005, Marco Pesenti Gritti
License: GPL-2+

Files: libdocument/ev-debug.c
 libdocument/ev-debug.h
Copyright: 2002-2005, Paolo Maggi
 2000, 2001, Chema Celorio, Paolo Maggi
 1998, 1999, Alex Roberts, Evan Lawrence
License: GPL-2+

Files: libdocument/ev-document-annotations.h
Copyright: 2007, Iñigo Martinez <inigomartinez@gmail.com>
License: GPL-2+

Files: libdocument/ev-document-factory.c
Copyright: 2018, Christian Persch
 2005, Red Hat, Inc.
License: GPL-2+

Files: libdocument/ev-document-factory.h
 libdocument/ev-document-find.c
 libdocument/ev-document-find.h
 libdocument/ev-document-fonts.c
 libdocument/ev-document-fonts.h
 libdocument/ev-document-links.c
 libdocument/ev-document-links.h
 libdocument/ev-document-security.c
 libdocument/ev-document-security.h
 libdocument/ev-link.c
 libdocument/ev-link.h
 libdocument/ev-selection.c
Copyright: 2004, 2005, 2011, 2012, 2015, 2016, Red Hat, Inc.
License: GPL-2+

Files: libdocument/ev-document-info.c
 libdocument/ev-document.c
Copyright: 2018, 2021, Christian Persch
 2009, Carlos Garcia Campos
 2004, Marco Pesenti Gritti
License: GPL-2+

Files: libdocument/ev-document-info.h
Copyright: 2021, Christian Persch
 2000-2003, Marco Pesenti Gritti
License: GPL-2+

Files: libdocument/ev-document-media.c
 libdocument/ev-document-media.h
 libdocument/ev-media.c
 libdocument/ev-media.h
Copyright: 2014, 2015, Igalia S.L.
License: GPL-2+

Files: libdocument/ev-document-misc.c
Copyright: 2009, Juanjo Marín <juanj.marin@juntadeandalucia.es>
 2007, Carlos Garcia Campos <carlosgc@gnome.org>
 2000-2003, Marco Pesenti Gritti
License: GPL-2+

Files: libdocument/ev-document-text.c
 libdocument/ev-document-text.h
Copyright: 2010, Yaco Sistemas, Daniel Garcia <danigm@yaco.es>
License: GPL-2+

Files: libdocument/ev-document.h
Copyright: 2009, Carlos Garcia Campos
 2000-2003, Marco Pesenti Gritti
License: GPL-2+

Files: libdocument/ev-file-exporter.c
 libdocument/ev-file-exporter.h
Copyright: 2003, 2004, Martin Kretzschmar
License: GPL-2+

Files: libdocument/ev-file-helpers.c
Copyright: 2009, Christian Persch
 2002, Jorn Baayen
License: GPL-2+

Files: libdocument/ev-file-helpers.h
Copyright: 2002, Jorn Baayen
License: GPL-2+

Files: libdocument/ev-form-field-private.h
Copyright: 2020, Germán Poo-Caamaño <gpoo@gnome.org>
License: GPL-2+

Files: libdocument/ev-form-field.c
Copyright: 2020, Germán Poo-Caamaño <gpoo@gnome.org>
 2007, Carlos Garcia Campos <carlosgc@gnome.org>
 2006, Julien Rebetez
License: GPL-2+

Files: libdocument/ev-form-field.h
Copyright: 2006, Julien Rebetez
License: GPL-2+

Files: libdocument/ev-init.c
 libdocument/ev-init.h
 libdocument/ev-macros.h
 libdocument/ev-portal.h
 libdocument/ev-version.h.in
Copyright: 2009, 2012, 2018, Christian Persch
License: LGPL-2.1+

Files: libdocument/ev-link-action.c
 libdocument/ev-link-action.h
 libdocument/ev-link-dest.c
 libdocument/ev-link-dest.h
Copyright: 2006, Carlos Garcia Campos <carlosgc@gnome.org>
 2005, Red Hat, Inc.
License: GPL-2+

Files: libdocument/ev-module.c
 libdocument/ev-module.h
Copyright: 2005, Paolo Maggi
License: GPL-2+

Files: libdocument/ev-portal.c
Copyright: no-info-found
License: LGPL-2.1+

Files: libdocument/ev-render-context.c
 libdocument/ev-render-context.h
Copyright: 2005, Jonathan Blandford <jrb@gnome.org>
License: GPL-2+

Files: libdocument/ev-transition-effect.c
 libdocument/ev-transition-effect.h
Copyright: 2007, Carlos Garnacho <carlos@imendio.com>
License: GPL-2+

Files: libdocument/ev-xmp.c
Copyright: 2018, Evangelos Rigas <erigas@rnd2.org>
 2009, Juanjo Marín <juanj.marin@juntadeandalucia.es>
 2004, Red Hat, Inc.
License: GPL-2+

Files: libdocument/ev-xmp.h
Copyright: 2014, 2021, Christian Persch
License: GPL-2+

Files: libmisc/*
Copyright: 2014, 2015, Igalia S.L.
License: GPL-2+

Files: libmisc/ev-page-action-widget.c
Copyright: 2018, Germán Poo-Caamaño
 2003, 2004, Marco Pesenti Gritti
 2003, 2004, Christian Persch
License: GPL-2+

Files: libmisc/ev-page-action-widget.h
Copyright: 2003, 2004, Marco Pesenti Gritti
 2003, 2004, Christian Persch
License: GPL-2+

Files: libview/*
Copyright: 2004, 2005, Red Hat, Inc
License: GPL-2+

Files: libview/ev-annotation-window.c
 libview/ev-annotation-window.h
Copyright: 2009, Carlos Garcia Campos <carlosgc@gnome.org>
 2007, Iñigo Martinez <inigomartinez@gmail.com>
License: GPL-2+

Files: libview/ev-color-contrast.c
 libview/ev-color-contrast.h
Copyright: 2020, Vanadiae <vanadiae35@gmail.com>
License: GPL-2+

Files: libview/ev-document-model.c
Copyright: 2009, Carlos Garcia Campos
 2005, Red Hat, Inc
License: GPL-2+

Files: libview/ev-document-model.h
 libview/ev-page-cache.c
 libview/ev-page-cache.h
Copyright: 2006, 2009, Carlos Garcia Campos
License: GPL-2+

Files: libview/ev-form-field-accessible.c
 libview/ev-form-field-accessible.h
 libview/ev-image-accessible.c
 libview/ev-image-accessible.h
Copyright: 2014, Igalia
License: GPL-2+

Files: libview/ev-job-scheduler.c
 libview/ev-job-scheduler.h
 libview/ev-link-accessible.c
 libview/ev-link-accessible.h
 libview/ev-print-operation.h
 libview/ev-view-presentation.c
 libview/ev-view-presentation.h
Copyright: 2006-2010, 2012, 2013, 2015, Carlos Garcia Campos <carlosgc@gnome.org>
License: GPL-2+

Files: libview/ev-jobs.c
 libview/ev-jobs.h
Copyright: 2008, Carlos Garcia Campos <carlosgc@gnome.org>
 2005, Red Hat, Inc
License: GPL-2+

Files: libview/ev-media-player.c
 libview/ev-media-player.h
 libview/ev-page-accessible.c
 libview/ev-page-accessible.h
Copyright: 2014, 2015, Igalia S.L.
License: GPL-2+

Files: libview/ev-print-operation.c
Copyright: 2018, 2021, Christian Persch
 2016, Red Hat, Inc.
 2008, Carlos Garcia Campos <carlosgc@gnome.org>
License: GPL-2+

Files: libview/ev-stock-icons.c
Copyright: 2018, Germán Poo-Caamaño <gpoo@gnome.org>
 2003, Martin Kretzschmar <Martin.Kretzschmar@inf.tu-dresden.de>
License: GPL-2+

Files: libview/ev-stock-icons.h
Copyright: 2003, 2004, Martin Kretzschmar
License: GPL-2+

Files: libview/ev-timeline.c
 libview/ev-timeline.h
 libview/ev-transition-animation.c
 libview/ev-transition-animation.h
Copyright: 2007, Carlos Garnacho <carlos@imendio.com>
License: LGPL-2+

Files: meson.build
Copyright: no-info-found
License: GPL-2

Files: org.gnome.Evince.appdata.xml.in
Copyright: 2014, 2021, Christian Persch
License: GPL-2+

Files: previewer/*
Copyright: 2014-2018, 2020, Germán Poo-Caamaño <gpoo@gnome.org>
 2007, 2008, 2010, 2012-2014, Carlos Garcia Campos <carlosgc@gnome.org>
License: GPL-2+

Files: previewer/ev-previewer-window.c
Copyright: 2018, Germán Poo-Caamaño <gpoo@gnome.org>
 2018, 2021, Christian Persch
 2009, Carlos Garcia Campos <carlosgc@gnome.org>
License: GPL-2+

Files: previewer/ev-previewer-window.h
Copyright: 2006-2010, 2012, 2013, 2015, Carlos Garcia Campos <carlosgc@gnome.org>
License: GPL-2+

Files: previewer/ev-previewer.c
Copyright: 2010, 2012, 2018, 2021, 2022, Christian Persch
 2009, Carlos Garcia Campos <carlosgc@gnome.org>
License: GPL-2+

Files: previewer/previewer.gresource.xml
Copyright: 2012, Christian Persch
License: GPL-3+

Files: properties/*
Copyright: 2004, 2005, Red Hat, Inc
License: GPL-2+

Files: properties/ev-properties-main.c
Copyright: 2005, Bastien Nocera <hadess@hadess.net>
 2003, Andrew Sobala <aes@gnome.org>
 2000, 2001, Eazel Inc.
License: GPL-2+ with GStreamer exception

Files: shell/*
Copyright: 2006-2010, 2012, 2013, 2015, Carlos Garcia Campos <carlosgc@gnome.org>
License: GPL-2+

Files: shell/ev-application.c
Copyright: 2010, 2012, Christian Persch
 2004, Martin Kretzschmar
License: GPL-2+

Files: shell/ev-application.h
 shell/ev-window.h
Copyright: 2003, 2004, Martin Kretzschmar
License: GPL-2+

Files: shell/ev-daemon.c
Copyright: 2010, 2012, 2018, 2021, 2022, Christian Persch
 2009, Carlos Garcia Campos <carlosgc@gnome.org>
License: GPL-2+

Files: shell/ev-find-sidebar.c
 shell/ev-find-sidebar.h
Copyright: 2013, Carlos Garcia Campos <carlosgc@gnome.org>
 2008, Sergey Pushkin <pushkinsv@gmail.com >
License: GPL-2+

Files: shell/ev-history.c
Copyright: 2018, Germán Poo-Caamaño <gpoo@gnome.org>
 2005, Marco Pesenti Gritti
License: GPL-2+

Files: shell/ev-history.h
 shell/ev-sidebar-page.c
 shell/ev-sidebar-page.h
 shell/main.c
Copyright: 2000-2005, Marco Pesenti Gritti
License: GPL-2+

Files: shell/ev-media-player-keys.c
Copyright: 2010, Christian Persch
 2008, Bastien Nocera <hadess@hadess.net>
 2007, Jan Arne Petersen <jap@gnome.org>
License: GPL-2+

Files: shell/ev-media-player-keys.h
Copyright: 2008, Bastien Nocera <hadess@hadess.net>
 2007, Jan Arne Petersen <jap@gnome.org>
License: GPL-2+

Files: shell/ev-message-area.c
 shell/ev-message-area.h
 shell/ev-progress-message-area.c
 shell/ev-progress-message-area.h
 shell/ev-sidebar-bookmarks.c
 shell/ev-toolbar.c
 shell/ev-zoom-action.c
Copyright: 2014-2018, 2020, Germán Poo-Caamaño <gpoo@gnome.org>
 2007, 2008, 2010, 2012-2014, Carlos Garcia Campos <carlosgc@gnome.org>
License: GPL-2+

Files: shell/ev-password-view.c
Copyright: 2018, Germán Poo-Caamaño <gpoo@gnome.org>
 2008, Carlos Garcia Campos <carlosgc@gnome.org>
 2005, Red Hat, Inc
License: GPL-2+

Files: shell/ev-password-view.h
 shell/ev-properties-dialog.c
 shell/ev-properties-dialog.h
 shell/ev-properties-fonts.c
 shell/ev-properties-fonts.h
 shell/ev-window-title.h
Copyright: 2004, 2005, Red Hat, Inc
License: GPL-2+

Files: shell/ev-properties-license.c
 shell/ev-properties-license.h
Copyright: 2009, Juanjo Marín <juanj.marin@juntadeandalucia.es>
 2005, Red Hat, Inc
License: GPL-2+

Files: shell/ev-recent-view.c
Copyright: 2014, Carlos Garcia Campos
 2013, Aakash Goenka
License: GPL-2+

Files: shell/ev-recent-view.h
Copyright: 2013, Aakash Goenka
License: GPL-2+

Files: shell/ev-sidebar-attachments.c
 shell/ev-sidebar-attachments.h
Copyright: 2006, 2009, Carlos Garcia Campos
License: GPL-2+

Files: shell/ev-sidebar-links.c
 shell/ev-sidebar-links.h
 shell/ev-sidebar-thumbnails.h
Copyright: 2004, 2005, 2011, 2012, 2015, 2016, Red Hat, Inc.
License: GPL-2+

Files: shell/ev-sidebar-thumbnails.c
Copyright: 2004, Red Hat, Inc.
 2004, 2005, Anders Carlsson <andersca@gnome.org>
License: GPL-2+

Files: shell/ev-sidebar.c
 shell/ev-sidebar.h
Copyright: 2018, Germán Poo-Caamaño <gpoo@gnome.org>
 2004, Red Hat, Inc.
License: GPL-2+

Files: shell/ev-utils.c
 shell/ev-utils.h
Copyright: 2004, Anders Carlsson <andersca@gnome.org>
License: GPL-2+

Files: shell/ev-window-title.c
Copyright: 2018, Germán Poo-Caamaño <gpoo@gnome.org>
 2005, Red Hat, Inc
License: GPL-2+

Files: shell/ev-window.c
Copyright: 2009, Juanjo Marín <juanj.marin@juntadeandalucia.es>
 2008, Carlos Garcia Campos
 2004, Red Hat, Inc.
 2004, Martin Kretzschmar
 2003-2005, 2009, 2012, Christian Persch
 2000-2004, Marco Pesenti Gritti
License: GPL-2+

Files: shell/evince-menus.ui
Copyright: 2014, Canonical Ltd.
 2012, Christian Persch
License: GPL-3+

Files: shell/evince.gresource.xml
Copyright: 2012, Christian Persch
License: GPL-3+

Files: thumbnailer/*
Copyright: 2005, Fernando Herrera <fherrera@onirica.com>
License: GPL-2+

